package com.example.company.skip_activity_stack

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class Activity3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_3)
    }
}
