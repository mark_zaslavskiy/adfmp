package com.example.company.myapplication

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.view.View.VISIBLE
import android.view.View.INVISIBLE

import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private fun createNotificationChannel(CHANNEL_ID: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "name"
            val descriptionText = "aaa"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            Log.d("Notification_TAG", "In")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("Notification_TAG", getString(R.string.my_button_label))

        createNotificationChannel("my_channel")

        val but1 = findViewById<Button>(R.id.button)

        button.setOnClickListener {
            Log.d("Notification_TAG", "clicked")

            progressBar.visibility = if (progressBar.visibility == VISIBLE )  INVISIBLE  else VISIBLE
        }

        button2.setOnClickListener {
            var mBuilder = NotificationCompat.Builder(this, "my_channel")
                .setSmallIcon(R.drawable.notification_icon_background)
                .setContentTitle("Hello")
                .setContentText("Hello!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            val notification = mBuilder.build()
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(1, notification)
        }

        button3.setOnClickListener {
            Toast.makeText(this, "Hello, I am a toast!", Toast.LENGTH_LONG).show()
        }
    }
}
