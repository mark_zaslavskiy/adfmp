package com.example.company.myapplication

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Rule
    @JvmField
    var mActivityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testButton(){


        onView(withId(R.id.editText2)).perform(
            clearText(),
            typeText("john,mary \n"),
            closeSoftKeyboard())

        onView(withId(R.id.btn1)).perform(click())

        onView(withId(R.id.textView)).check(
            matches(withText("john,mary ")))
    }

}
