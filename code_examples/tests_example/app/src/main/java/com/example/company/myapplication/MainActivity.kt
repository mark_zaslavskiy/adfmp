package com.example.company.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock.elapsedRealtimeNanos
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


class MainActivity : AppCompatActivity() {

    private val TAG = "SENSOR_DEMO"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn1: Button = findViewById<Button>(R.id.btn1)
        val btn2: Button = findViewById<Button>(R.id.btn2)
        val textView: TextView = findViewById<TextView>(R.id.textView)
        val editText: EditText = findViewById<EditText>(R.id.editText2)
        /// Test part
        btn1.setOnClickListener {
            var a = elapsedRealtimeNanos()
            var b = elapsedRealtimeNanos()
            Log.d("tag", "Diff1 ${b-a}")
            var str = ""
            a = elapsedRealtimeNanos()
            for ( i in 0..1000) str += " "+(i*i).toString()
            b = elapsedRealtimeNanos()
            Log.d("tag", "Diff2 ${b-a}")
            textView.text = editText.text

        }

        btn2.setOnClickListener {
            Log.d("tag", "btn2: I do nothing")
        }
    }
}
