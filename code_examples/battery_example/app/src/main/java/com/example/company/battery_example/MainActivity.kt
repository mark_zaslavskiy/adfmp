package com.example.company.battery_example

import android.os.BatteryManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bm:BatteryManager = (getSystemService(BATTERY_SERVICE) as BatteryManager);

        button.setOnClickListener {


        /*  BATTERY_PROPERTY_CHARGE_COUNTER   Remaining battery capacity in microampere-hours
            BATTERY_PROPERTY_CURRENT_NOW      Instantaneous battery current in microamperes
            BATTERY_PROPERTY_CURRENT_AVERAGE  Average battery current in microamperes
            BATTERY_PROPERTY_CAPACITY         Remaining battery capacity as an integer percentage
            BATTERY_PROPERTY_ENERGY_COUNTER   Remaining energy in nanowatt-hours*/
            val currentNow: Int = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW)
            val capacity: Int = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
            val remaining: Long = bm.getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER)

            val batteryState = "currentNow = ${currentNow} mA, capacity = ${capacity}%, remaining = ${remaining} nWh"
            textView.text = batteryState
            Log.d("Battery_example", batteryState)

        }
    }
}
