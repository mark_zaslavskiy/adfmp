package com.example.app_skeleton3a.utils;

public enum LogLevel {
	INFO, DEBUG, WARNING, ERROR, FATAL
}
