package com.example.app_skeleton3a.utils;

public class BaseAppSceletonException extends Exception {
	public enum Colors{
		RED, GREEN, BLUE, CYAN, WHITE, BLACK, IVORY, ORANGE, YELLOW }

	public enum Animals{
		CHEETAH, CAT, DOG, RAT, MICE, ELEPHANT, EAGLE, PIGEON, LYNX, TIGER 
	}
	
	// Assume that we have two digits error codes 
	private static String generateMnemonicCode(int numberCode){
		int firstDigit = String.valueOf(numberCode).charAt(0);
		int secondDigit = String.valueOf(numberCode).charAt(1);
		
		return Colors.values()[firstDigit] + "" + Animals.values()[secondDigit] + 
				" error: " +  ErrorList.getErrorByCode(numberCode);
	}
}
