package com.example.app_skeleton3a.utils;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.util.Log;

public class CustomLogger {
	
	
	private static final String APP_NAME = "APP_SKELETON";
	private static Context s_context = null;
	private static boolean s_enabled = true;
    
	public static void initLogger(Context context, boolean enabled){
		CustomLogger.s_context = context;
		CustomLogger.s_enabled = enabled;
	}
	
	public static boolean isLoggingEnabled(){
		return s_enabled ;
	}	
	
	// Log extended message into LogCat and (if enabled) to log file
	public static void log(LogLevel level, String message){		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		String time = dateFormat.format(date);
		// Machine readable format (better to use JSON/XML or something more common
		String logMessage = "Level:" + level.toString() + "," +
				"Line:" + Integer.toString(CustomLogger.getLineNumber()) + "," +
				"Message:" + message  + "," +
				"Time:" + time;
		
		if (CustomLogger.isLoggingEnabled()){
			writeEntryToFile(logMessage);
		}
		
		// Duplicate entry to eclipse log for debug purposes
		Log.d(APP_NAME, logMessage);
		
	}
	
	private static void writeEntryToFile(String logMessage) {
		String filename = CustomLogger.generateLogFileName();
		FileOutputStream outputStream;

		try {
		  outputStream = CustomLogger.s_context .openFileOutput(filename, Context.MODE_PRIVATE);
		  outputStream.write(logMessage.getBytes());
		  outputStream.close();
		} catch (Exception e) {
		  e.printStackTrace();
		}
		CustomLogger.logRotate();
	}

	
	private static String generateLogFileName() {
		// TODO generate log file name containing date info
		return "Log_file";
	}

	// Perform log rotation for keeping only N last days logs zipped
	private static void logRotate() {
		// TODO
	}

	// Return current line number
	public static int getLineNumber() {
	    return Thread.currentThread().getStackTrace()[2].getLineNumber();
	}
	
}
