package com.example.app_skeleton3a.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class ErrorList {
	static Map<Integer, String> s_errorList;
	static void initErrorList(){
		Map<Integer, String> tmp = new LinkedHashMap<Integer, String>();
		tmp.put(10, "Read Error");
		tmp.put(20, "Write Error");
		s_errorList = Collections.unmodifiableMap(tmp);
	}
	public static String getErrorByCode(int numberCode) {
		return s_errorList.get(numberCode);
	}

	
}

