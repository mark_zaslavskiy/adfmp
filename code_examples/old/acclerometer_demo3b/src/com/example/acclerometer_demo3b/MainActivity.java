package com.example.acclerometer_demo3b;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

// implements SensorEventListener need to make activity recieving signals from sensor
public class MainActivity extends Activity implements SensorEventListener{

	private static final String ACCELEROMETER_EXAMPLE = "AccelerometerExample";
	private SensorManager m_sensorManager;
	private Sensor m_accelerometerSensor;
	private TextView xView;
	private TextView yView;
	private TextView zView;
	
	@Override	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setupView();
		
		m_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		// Aquiring accelerometer
		if (m_sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
			// Success
			Log.d(ACCELEROMETER_EXAMPLE, "Accelerometer aquired successfuly!");
			
			m_accelerometerSensor = m_sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			m_sensorManager.registerListener(this, m_accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
		} else {
			// Fail
			Log.e(ACCELEROMETER_EXAMPLE, "Unable to aquire accelerometer");
		}
	}
	
	// Setup view for acceleration components
	private void setupView() {
		xView = (TextView) findViewById(R.id.xView);
		yView = (TextView) findViewById(R.id.yView);
		zView = (TextView) findViewById(R.id.zView);		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// THis callback will be called on which accelerometer update
		Log.d(ACCELEROMETER_EXAMPLE, "Recieved acceleration data");
		xView.setText(Float.toString(event.values[0]));
		yView.setText(Float.toString(event.values[0]));
		zView.setText(Float.toString(event.values[0]));

	}
	
	// STUBS
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// Empty due to persitant accuracy of accelerometer		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static int ExampleTestableFunction(int a, int b){
		return a+b;
	}
}
