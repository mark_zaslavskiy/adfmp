package com.example.acclerometer_demo3b.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(exampleTest.class);
		//$JUnit-END$
		return suite;
	}

}
